<?php

namespace App\Exception;

use App\HttpController\Api\Base;
use App\Log\LogHandel;
use EasySwoole\Http\Request;
use EasySwoole\Http\Response;

class ExceptionHandler
{
    public static function handle(\Throwable $exception, Request $request, Response $response)
    {

        $code = $exception->getCode();
        $msg = $exception->getMessage();

//      自定义异常处理 存LOG
        $log = new LogHandel();
        $logs = $request->getServerParams()['remote_addr'] . '  ' . $msg . '  ' . $exception->getFile() . '  ' . $exception->getLine();
        $log->log($logs);

        //页面返回错误信息JSON格式
        $data = [
            'code' => $code,
            'msg' => $msg,
        ];
        $result = json_encode($data, JSON_UNESCAPED_UNICODE);
        return $response->withHeader("Content-Type", "application/json;charset=UTF-8")
            ->withHeader("Access-Control-Allow-Origin", "*")
            ->write($result);
    }


}